package org.example;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Main {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("hello");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        try{
            Member findMamber = em.find(Member.class, 1L);
            System.out.println(findMamber.getId());
            System.out.println(findMamber.getName());
//            Member member = new Member();
//            member.setId(1L);
//            member.setName("helloA");
//
//            em.persist(member); // Member 객체를 영속화하고 데이터베이스에 저장
            tx.commit();
        }catch (Exception e) {
            tx.rollback();
            em.close();
        }
        emf.close();

    }
}